<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Portfolio</title>

    <link rel="stylesheet" href="./style.css">

</head>

<?php include 'data.php'; ?>

<body>
    <img src="./asset/banner.jpg" alt="background" id="background">

    <nav>
        <ul>
            <li><a class="onglet" href="index.php">Acceuil</a></li>
            <li><a class="onglet" href="folio.php">PortFolio</a></li>
            <li><a class="onglet" href="contact.php">Contact</a></li>
            <li><a id="formLien" href="/PortFolioREMAKE/form.php">Gestion</a></li>
        </ul>
    </nav>