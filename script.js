let projets = document.getElementsByClassName("projet")

for (let proj of projets) {
    proj.addEventListener('click',ecran);
    
}


function ecran(event) {
    
    event.preventDefault();
    
    let projets = document.getElementsByClassName("projet");
    for (let projet of projets) {
        projet.classList.remove("slide");
    }
  
    let id = event.target.id;
    let projet = document.getElementById(id);
    projet.classList.add("slide")

    let contenue = document.getElementById("ecran");
    while (contenue.firstChild) {
        contenue.removeChild(contenue.firstChild);
    }
  
    let selection = document.getElementById("info_" + id);
   
    selectClone = selection.cloneNode(true);
    selectClone.classList.remove("projetInfosOFF");
    selectClone.classList.add("projetInfosON");
    selectClone.classList.add("css-typing");
    document.getElementById("ecran").appendChild(selectClone);

    console.log(event);
}


// _________________________________________________________________________________________________
