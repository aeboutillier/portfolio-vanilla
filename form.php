<?php
 
session_start();
if ($_SESSION["connecter"] != "ok") {
header("location:login.php");
exit();
}
?>



    <?php include 'header.php'; ?>
    
    <div class="containerForm">
        <img src="./asset/alu.webp" alt="backgroundForm" id="backgroundForm">
        <div class="form_box">
            <form action="insert.php" method="POST">
                <label for="nom">Nom Du Projet :</label>
                <input type="text" name="nom" id="nom">
                <label for="URL">URL Du Projet :</label>
                <input type="text" name="URL" id="URL">
                <label for="desc">Description :</label>
                <input type="text" name="desc" id="desc">
                <label for="tech">Technologie :</label>
                <div class="checkbox">

                    <?php
        
                    $checkboxTech = recupTech();
                    foreach ($checkboxTech as $e){ ?>
                    <label for="checkbox"> <?php echo $e["nom"]?>
                        <input type="checkbox" name="<?php echo $e["nom"]?>">
                    </label>

                    <?php } ?>

                </div>
                <input type="submit" value="Ajouter">
            </form>
        </div>

        <div class="form_box">
            <form action="insert.php" method="POST">
                <label for="addTech">Ajouter une Technologie :</label>
                <input type="text" name="addTech" id="addTech">
                <input type="submit" value="Ajouter">
            </form>
            <a id="btnDeco" href="deconnexion.php">Se déconnecter</a>
        </div>

    </div>
</body>

</html>