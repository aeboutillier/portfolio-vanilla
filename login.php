<?php

// "select * from utilisateurs where nom=? and pass=? limit 1");
 
session_start();
include("infos.php");
@$pseudo = $_POST["pseudo"];
@$password = $_POST["password"];
@$valider = $_POST["valider"];
$erreur = "";

if (isset($valider)) {
    include("data.php");
    $query = "SELECT * FROM logAdmin WHERE nom = ? AND pass = ?";
    $verify = $pdo->prepare($query);
    $verify->execute(array($pseudo, $password));
    $user = $verify->fetchAll();
    if (count($user) > 0) {
        $_SESSION["connecter"] = "ok";
        header("location:form.php");
    } else {
        $erreur = "Mauvais login ou mot de passe!";
    }
}
?>


    <?php include 'header.php'; ?>
    
    <div class="containerLog">
        <img src="./asset/alu.webp" alt="backgroundForm" id="backgroundForm">
        <h1>Authentification</h1>
        <form name="form" method="post" action="">
            <label for="pseudo">Nom d'Utilisateur</label>
            <input autocomplete="off" type="text" name="pseudo" placeholder="Votre Pseudo" />
            <label for="password">Mot De Passe</label>
            <input autocomplete="off" type="password" name="password" placeholder="Mot de passe" />
            <input type="submit" name="valider" id="btn" value="S'authentifier" />
        </form>
        <div class="erreur"><?php  echo  $erreur  ?></div>
    </div>
</body>

</html>