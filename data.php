<?php

// $host = "localhost";
// $dbname = "folio";
// $username = "admin";
// $password = "admin";

try{
   $pdo = new PDO('mysql:host=localhost;dbname=folio','admin','admin');
   // Activation des erreurs PDO
   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   // mode de fetch par défaut : FETCH_ASSOC / FETCH_OBJ / FETCH_BOTH
   $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
 }
 catch(PDOException $e){
   die('Erreur : ' . $e->getMessage());
 }

function recupProjet() {
   global $pdo;
   $req = $pdo->query("SELECT * FROM projet");
   return $req->fetchAll();
}

function insertProjet($nom, $p_url, $p_desc, $tech) {
   global $pdo;
   $sql = "INSERT INTO projet (nom, p_url, p_desc) VALUES (?,?,?)";
   try{
      $pdo->prepare($sql)->execute([$nom, $p_url, $p_desc]);
    }catch(Exception $e){
      // en cas d'erreur :
       echo " Erreur ! ".$e->getMessage();
       echo " Les datas : " ;
      print_r($datas);
    }

   $id_projet = $pdo->lastInsertId();
   
   foreach ($tech as $id_tech) {
      $sql = "INSERT INTO p_tech(id_projet,id_tech) VALUES (?,?)";
      $pdo->prepare($sql)->execute([$id_projet, $id_tech]);
   }
   header("location: index.php");

}

function recupTech() {
   global $pdo;
   $req = $pdo->query("SELECT * FROM tech");

   return $req->fetchAll();
}

function insertTech($addTech) {
   global $pdo;
   $sql = "INSERT INTO tech (nom) VALUES (?)";
   $pdo->prepare($sql)->execute([$addTech]);
   header("location: form.php");
}

//_________________Recup Tables de Liaison________________//

function recupP_tech($idT) {
   global $pdo;
   $req = $pdo->query("SELECT nom FROM tech JOIN p_tech WHERE tech.id = p_tech.id_tech AND p_tech.id_projet = $idT");
   return $req->fetchAll();
}

function deleteProjet($id) {
   global $pdo;
   $req = $pdo->query("DELETE FROM projet WHERE id= $id");
}


?>