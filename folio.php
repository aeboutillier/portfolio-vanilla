    <?php include 'header.php'; ?>

    <div class="container">  
        <div class="boxleft">
            <div class='liste'>
                <ul>
                    <?php
                    $projet = recupProjet();

                    foreach ($projet as $e) {
                    
                        $url = $e['p_url'];
                        $idT = $e['id'];
                        $recupTech = recupP_tech($idT);

                    ?>
                        <li>
                            <div class="projet" id="projet_<?php echo $e['id']; ?>">

                                <p class="pProj">Projet <?php echo $e['id']; ?> : </p> 
                                <span> <?php echo $e['nom']; ?> </span>
                                                                
                                <div class="projetInfosOFF" id="info_projet_<?php echo $e['id']; ?>">
                                    <p> Description : <?php echo $e['p_desc']; ?></p>
                                    <p> Technos : 
                                        <?php 
                                        foreach ($recupTech as $e) {
                                            ?>
                                            <span><?php echo $e["nom"]; ?></span>
                                            <?php
                                        } 
                                        ?>
                                    </p>
                                    <a class="projetLien" href="<?php echo $url;?>">Voir -> </a>
                                </div>  
                            </div> 
                        </li>

                    <?php
                    }
                    ?>    
                </ul>
            </div>
        </div>

        <div class="boxright">
            <img src="./asset/fondportable2.png" alt="fond" id="fond">  
            <div id="ecran">
                <p class="text">Selectionner un projet...</p>
            </div>
        </div>
    </div>  

    <script src="script.js"></script>
</body>


</html>