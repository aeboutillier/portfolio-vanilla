
DROP DATABASE IF EXISTS folio;
CREATE DATABASE folio;

USE folio;

GRANT ALL PRIVILEGES ON folio.* TO 'admin'@'localhost';

CREATE TABLE projet (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(20) NOT NULL,
p_url VARCHAR(300) NOT NULL,
p_desc VARCHAR(30) NOT NULL
);

CREATE TABLE tech (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(20) NOT NULL
);

CREATE TABLE p_tech (
id_projet INT UNSIGNED,
id_tech INT UNSIGNED,
UNIQUE (id_projet,id_tech),
FOREIGN KEY (id_projet) REFERENCES projet(id),
FOREIGN KEY (id_tech) REFERENCES tech(id)
);

CREATE TABLE logAdmin (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(20) NOT NULL,
pass VARCHAR(300) NOT NULL
);


INSERT INTO projet (nom,p_url,p_desc)
VALUES (
    'TEST',
    'https://www.testdebit.fr/',
    'Testeur De Debit'
    );
    

INSERT INTO tech (nom)
VALUES ('HTML'),('CSS'),('PHP'),('JS');
